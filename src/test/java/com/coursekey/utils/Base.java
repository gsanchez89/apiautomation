package com.coursekey.utils;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import com.thecoursekey.common.Constants;
import com.thecoursekey.common.ExtentProperties;
import com.thecoursekey.common.PropertyManager;
import com.thecoursekey.common.RestUtils;
import com.thecoursekey.models.auth.Login;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.io.File;

public class Base {
    public RequestSpecification reqSpec;
    public ResponseSpecification resSpec;
    protected Response respValid;
    protected Login authUser;
    protected ExtentProperties exProp = new ExtentProperties();
    String extentReportFile;

    @BeforeTest
    @Parameters("user")
    public void setup(String user) throws Exception {
        exProp = new ExtentProperties();
        resSpec = RestUtils.getResponseSpecification();
        authUser = new Login();
        extentReportFile = Constants.HTML_REPORT;

        // Create object of extent report and specify the report file path.
        exProp.setExtent(new ExtentReports(extentReportFile, false));

        exProp.getExtent().loadConfig(new File(Constants.EXTENT_CONFIG));

        switch (user.toLowerCase()){
            case "instructor":
                authUser.setEmail(PropertyManager.getInstance().getProperty("instructoremail"));
                authUser.setPassword(PropertyManager.getInstance().getProperty("instructorpass"));
                break;
            case "student":
                authUser.setEmail(PropertyManager.getInstance().getProperty("studentemail"));
                authUser.setPassword(PropertyManager.getInstance().getProperty("studentpass"));
                break;
            default:
                throw new Exception("No defined user");
        }
    }

    @AfterMethod
    public void getResult(ITestResult result){
        switch (result.getStatus()){
            case ITestResult.FAILURE:
                logFailStep(result.getThrowable().toString()+ ".\n The Response is: " + respValid.getBody().asString()) ;
                logTestFail(result);
                break;
            case ITestResult.SUCCESS:
                logTestPass(result);
                break;
            case ITestResult.SKIP:
                logTestSkip(result);
                break;
        }
    }

    @AfterTest
    public void tearDown(){
        exProp.getExtentTest().log(LogStatus.INFO, "Execution Completed!");
        // close report.
        exProp.getExtent().endTest(exProp.getExtentTest());
        // writing everything to document.
        exProp.getExtent().flush();
    }

    public void startTestCase(String testName, String desc){
        exProp.setExtentTest(exProp.getExtent().startTest(testName, desc));
    }

    private void logTestPass(ITestResult result){
        exProp.getExtentTest().log(LogStatus.PASS, result.getName() + " Passed");
    }

    private void logTestFail(ITestResult result){
        exProp.getExtentTest().log(LogStatus.FAIL, result.getName() + " Failed" );
    }

    private void logTestSkip(ITestResult result){
        exProp.getExtentTest().log(LogStatus.SKIP, result.getName() + " Skipped");
    }

    public void logInfo(String details){
        exProp.getExtentTest().log(LogStatus.INFO, details );
    }

    public void logPassStep(String details){
        exProp.getExtentTest().log(LogStatus.PASS, details);
    }

    public void logFailStep(String details) { exProp.getExtentTest().log(LogStatus.FAIL, details); }
}
