package com.coursekey.coursekeyapi;

import com.coursekey.properties.auth.ResetPasswordData;
import com.coursekey.properties.auth.SignUpProps;
import com.coursekey.properties.auth.ValidationEmailData;
import com.coursekey.utils.Base;
import com.thecoursekey.common.Constants;
import com.thecoursekey.common.RequestEnum;
import com.thecoursekey.common.RestUtils;
import com.thecoursekey.models.auth.ResetPassword;
import com.thecoursekey.models.auth.SignUp;
import com.thecoursekey.models.auth.ValidationEmail;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class AuthService extends Base {

    private String loginEndpoint = "login/";
    private String logoutEndpoint = "logout/";
    private String signupEndpoint = "signup/";
    private String validationEmailEndpoint ="validate/";
    private String resetPasswordEndpoint = "resetpassword/";
    private String forgotPasswordEndpoint = "forgotpassword/";
    private String securityQuestionEndpoint = "question/";
    private String resendVerificationEndpoint = "resendverification/";
    private String forgotemailEndpoint = "forgotemail";
    private String policyEndpoint = "policy";
    private Map<String, Object> login = new HashMap<>();
    private SignUp signup;
    private ValidationEmail validationemail;
    private ResetPassword resetPassword;



    @Test(priority = 0)
    public void postSuccessLogin(){
        //Start your Test Case for reporting
        startTestCase("Auth - Login: POST 200","Success");

        // Add the reqSpec if you need it
        reqSpec = RestUtils.getRequestSpecification();
        login.put("email", authUser.getEmail());
        login.put("password", authUser.getPassword());

        // Set your endpoint
        RestUtils.setEndPoint(Constants.AUTH + loginEndpoint);

        // Get the response
        respValid = RestUtils.getResponse(RestUtils.createFormParams(
                        reqSpec.contentType(ContentType.URLENC.withCharset("UTF-8")), login), RequestEnum.POST
        );

        // Validate status code 200
        respValid.then().log().all().statusCode(HttpStatus.SC_OK);

        // Log the response into the HTMl file
        logPassStep(respValid.getBody().asString());
        login.clear();
    }

    @Test(priority = 1)
    public void postUnauthorizedLogin(){
        startTestCase("Auth - Login: POST 401","Unauthorized - Invalid email or password");

        reqSpec = RestUtils.getRequestSpecification();
        login.put("email", authUser.getEmail());
        login.put("password", "Wrong Pass");

        RestUtils.setEndPoint(Constants.AUTH + loginEndpoint);
        respValid = RestUtils.getResponse(RestUtils.createFormParams(
                        reqSpec.contentType(ContentType.URLENC.withCharset("UTF-8")), login), RequestEnum.POST
        );

        // Validate status code 401
        respValid.then().log().all().statusCode(HttpStatus.SC_UNAUTHORIZED);
        logPassStep("Unauthorized - Invalid email or password");
        logPassStep(respValid.getBody().asString());
        login.clear();
    }

    @Test(priority = 2)
    public void postServerErrorLogin(){
        startTestCase("Auth - Login: POST 500","Server Error - Let us know");

        reqSpec = RestUtils.getRequestSpecification();
        respValid = RestUtils.getResponse(RestUtils.createFormParams(
                reqSpec.contentType(ContentType.URLENC), login), RequestEnum.POST
        );

        // Validate status code 500
        respValid.then().log().all().statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        logPassStep(respValid.getBody().asString());
        login.clear();
    }

    @Test(dependsOnMethods = {"postSuccessLogin"})
    public void deleteLogout(){
        startTestCase("Auth - Logout: POST 200","Success");

        reqSpec = RestUtils.getRequestSpecification();
        RestUtils.setEndPoint(Constants.AUTH + logoutEndpoint);
        respValid = RestUtils.getResponse(reqSpec, RequestEnum.DELETE);

        // Validate status code 200
        respValid.then().log().all().statusCode(HttpStatus.SC_OK);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 3)
    public void postSuccessSignUp(){
        startTestCase("Auth - Sign Up: POST 200","Success");

        reqSpec = RestUtils.getRequestSpecification();
        signup = SignUpProps.signUp(true,"1151", false);

        RestUtils.setEndPoint(Constants.AUTH + signupEndpoint);

        respValid = RestUtils.getResponse(
                reqSpec.contentType(ContentType.JSON).body(signup), RequestEnum.POST
        );



        // Validate status code 200
        respValid.then().log().all().statusCode(HttpStatus.SC_OK);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 4)
    public void postBadRequestSignUp(){
        startTestCase("Auth - Sign Up: POST 400","Bad request - Invalid parameters");
        reqSpec = RestUtils.getRequestSpecification();
        signup = SignUpProps.signUp400("1151");

        respValid = RestUtils.getResponse(
                reqSpec.contentType(ContentType.JSON).body(signup), RequestEnum.POST
        );

        // Validate status code 400
        respValid.then().log().all().statusCode(HttpStatus.SC_BAD_REQUEST);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 5)
    public void postServerErrorSignUp(){
        startTestCase("Auth - Sign Up: POST 500","Server Error - Let us know");

        reqSpec = RestUtils.getRequestSpecification();

        respValid = RestUtils.getResponse(
                reqSpec.contentType(ContentType.JSON).body(SignUpProps.signUp500()), RequestEnum.POST
        );

        // Validate status code 500
        respValid.then().log().all().statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        logPassStep(respValid.getBody().asString());
    }

    //This one fails because you need to get the verificationCode of a non validated user from the dataBase
    @Test(priority = 6)
    public void postSuccessValidationEmail(){
        startTestCase("Auth - Validation Email: POST 200","Success");

        reqSpec = RestUtils.getRequestSpecification();
        validationemail = ValidationEmailData.validationemail();

        RestUtils.setEndPoint(Constants.AUTH + validationEmailEndpoint);

        respValid = RestUtils.getResponse(
                reqSpec.contentType(ContentType.JSON).body(validationemail), RequestEnum.POST
        );

        // Validate status code 200
        respValid.then().log().all().statusCode(HttpStatus.SC_OK);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 7)
    public void postUnauthorizedValidationEmail(){
        startTestCase("Auth - Validation Email: POST 401","Unauthorized");

        reqSpec = RestUtils.getRequestSpecification();
        validationemail = ValidationEmailData.validationemail401();

        respValid = RestUtils.getResponse(
                reqSpec.contentType(ContentType.JSON).body(validationemail), RequestEnum.POST
        );

        // Validate status code 401
        respValid.then().log().all().statusCode(HttpStatus.SC_UNAUTHORIZED);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 8)
    public void postBadRequestValidationEmail(){
        startTestCase("Auth - Validation Email: POST 400","Bad Request");

        reqSpec = RestUtils.getRequestSpecification();

        respValid = RestUtils.getResponse(
                reqSpec.contentType(ContentType.JSON).body(ValidationEmailData.ValidationEmail400()), RequestEnum.POST
        );

        // Validate status code 400
        respValid.then().log().all().statusCode(HttpStatus.SC_BAD_REQUEST);
        logPassStep(respValid.getBody().asString());
        login.clear();
    }

    @Test(priority = 9)
    public void postNotFoundValidationEmail(){
        startTestCase("Auth - Validation Email: POST 404","Not Found");

        reqSpec = RestUtils.getRequestSpecification();

        RestUtils.setEndPoint(Constants.AUTH + validationEmailEndpoint +"/");

        respValid = RestUtils.getResponse(
                reqSpec.contentType(ContentType.JSON).body(validationemail), RequestEnum.POST
        );

        // Validate status code 404
        respValid.then().log().all().statusCode(HttpStatus.SC_NOT_FOUND);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 10)
    public void postSuccessResetPassword(){
        startTestCase("Auth - Reset Password: POST 200","Success");

        reqSpec = RestUtils.getRequestSpecification();
        resetPassword = ResetPasswordData.resetPassword("Test619","Test619");


        RestUtils.setEndPoint(Constants.AUTH + resetPasswordEndpoint +"3386");

        respValid = RestUtils.getResponse(
                reqSpec.contentType(ContentType.JSON).body(resetPassword), RequestEnum.POST
        );

        // Validate status code 200
        respValid.then().log().all().statusCode(HttpStatus.SC_OK);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 11)
    public void postBadRequestResetPassword(){
        startTestCase("Auth - Reset Password: POST 400","Bad Request");

        reqSpec = RestUtils.getRequestSpecification();
        resetPassword = ResetPasswordData.resetPassword400();

        respValid = RestUtils.getResponse(
                reqSpec.contentType(ContentType.JSON).body(resetPassword), RequestEnum.POST
        );

        // Validate status code 400
        respValid.then().log().all().statusCode(HttpStatus.SC_BAD_REQUEST);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 11)
    public void postNotFoundResetPassword(){
        startTestCase("Auth - Reset Password: POST 404","Not Found");

        reqSpec = RestUtils.getRequestSpecification();
        RestUtils.setEndPoint(Constants.AUTH + resetPasswordEndpoint);


        respValid = RestUtils.getResponse(
                reqSpec.contentType(ContentType.JSON).body(resetPassword), RequestEnum.POST
        );

        // Validate status code 400
        respValid.then().log().all().statusCode(HttpStatus.SC_NOT_FOUND);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 12)
    public void getSuccessForgotPassword(){
        startTestCase("Auth - Forgot Password: GET 200","Success");

        reqSpec = RestUtils.getRequestSpecification();
        RestUtils.setEndPoint(Constants.AUTH + forgotPasswordEndpoint + "mariana@gmail.com");

        respValid = RestUtils.getResponse(reqSpec,RequestEnum.GET);
        // Validate status code 200
        respValid.then().log().all().statusCode(HttpStatus.SC_OK);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 13)
    public void getBadRequestForgotPassword(){
        startTestCase("Auth - Forgot Password: GET 400","Bad Request");

        reqSpec = RestUtils.getRequestSpecification();
        RestUtils.setEndPoint(Constants.AUTH + forgotPasswordEndpoint+"mar");


        respValid = RestUtils.getResponse(reqSpec,RequestEnum.GET);
        // Validate status code 400
        respValid.then().log().all().statusCode(HttpStatus.SC_BAD_REQUEST);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 14)
    public void getSuccessUserSecurityQuestion(){
        startTestCase("Auth - User Security Question: GET 200","Success");

        reqSpec = RestUtils.getRequestSpecification();
        RestUtils.setEndPoint(Constants.AUTH + securityQuestionEndpoint + "user");
        reqSpec.queryParam("firstName","pancho");
        reqSpec.queryParam("lastName","villa");
        reqSpec.queryParam("issuedID","1234");

        respValid= RestUtils.getResponse(reqSpec,RequestEnum.GET);

        // Validate status code 200
        respValid.then().log().all().statusCode(HttpStatus.SC_OK);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 15)
    public void getBadRequestUserSecurityQuestion(){
        startTestCase("Auth - User Security Question: GET 400","Bad Request");

        reqSpec = RestUtils.getRequestSpecification();
        reqSpec.queryParam("firstName","pancho");
        reqSpec.queryParam("lastName","villa");
        reqSpec.queryParam("issuedID","1234500");

        respValid= RestUtils.getResponse(reqSpec,RequestEnum.GET);

        // Validate status code 400
        respValid.then().log().all().statusCode(HttpStatus.SC_BAD_REQUEST);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 16)
    public void getSuccessSecurityQuestion(){
        startTestCase("Auth - All Security Questions: GET 200","Success");

        reqSpec = RestUtils.getRequestSpecification();
        RestUtils.setEndPoint(Constants.AUTH + securityQuestionEndpoint);

        respValid= RestUtils.getResponse(reqSpec,RequestEnum.GET);

        // Validate status code 200
        respValid.then().log().all().statusCode(HttpStatus.SC_OK);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 17)
    public void getSuccessResendVerification(){
        startTestCase("Auth - Resend Verification: GET 200","Success");

        reqSpec = RestUtils.getRequestSpecification();
        RestUtils.setEndPoint(Constants.AUTH + resendVerificationEndpoint + "mariana.lovegood@gmail.com");

        respValid= RestUtils.getResponse(reqSpec,RequestEnum.GET);

        // Validate status code 200
        respValid.then().log().all().statusCode(HttpStatus.SC_OK);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 18)
    public void getBadRequestResendVerification(){
        startTestCase("Auth - Resend Verification: GET 400","Bad Request");

        reqSpec = RestUtils.getRequestSpecification();
        RestUtils.setEndPoint(Constants.AUTH + resendVerificationEndpoint + "mar");

        respValid= RestUtils.getResponse(reqSpec,RequestEnum.GET);

        // Validate status code 400
        respValid.then().log().all().statusCode(HttpStatus.SC_BAD_REQUEST);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 19)
    public void getSuccessForgotEmail(){
        startTestCase("Auth - Forgot Email: GET 200","Success");

        reqSpec = RestUtils.getRequestSpecification();
        RestUtils.setEndPoint(Constants.AUTH + forgotemailEndpoint);

        reqSpec.queryParam("userID","5295");
        reqSpec.queryParam("answer","1111");

        respValid= RestUtils.getResponse(reqSpec,RequestEnum.GET);

        // Validate status code 200
        respValid.then().log().all().statusCode(HttpStatus.SC_OK);
        logPassStep(respValid.getBody().asString());
    }

    @Test(priority = 20)
    public void getBadRequestForgotEmail(){
        startTestCase("Auth - Forgot Email: GET 400","Bad Request");

        reqSpec = RestUtils.getRequestSpecification();
        RestUtils.setEndPoint(Constants.AUTH + forgotemailEndpoint);

        reqSpec.queryParam("userID","5295");
        reqSpec.queryParam("answer","112211");

        respValid= RestUtils.getResponse(reqSpec,RequestEnum.GET);

        // Validate status code 400
        respValid.then().log().all().statusCode(HttpStatus.SC_BAD_REQUEST);
        logPassStep(respValid.getBody().asString());
    }




}
