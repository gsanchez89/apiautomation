package com.coursekey.properties.auth;

import com.thecoursekey.models.auth.ValidationEmail;

public class ValidationEmailData {

    public static ValidationEmail validationemail() {
        ValidationEmail validationemail= new ValidationEmail();
        validationemail.setUserID(353);
        validationemail.setVerificationCode("wk9w46g0pjsjor");
        return validationemail;
    }

    public static ValidationEmail validationemail401() {
        ValidationEmail validationemail= new ValidationEmail();
        validationemail.setUserID(356);
        validationemail.setVerificationCode("i9pw3uhj8aor");
        return validationemail;
    }

    public static String ValidationEmail400(){
        String requestInternalServerError =
                "{" +
                        "\"userID\":\"aaaa\"," +
                        "\"verificationCode\":\"verificationCode\"" +
                        "}";
        return requestInternalServerError;
    }
}
