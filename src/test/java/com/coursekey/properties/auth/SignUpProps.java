package com.coursekey.properties.auth;

import com.thecoursekey.models.auth.SignUp;

import java.util.Random;

public class SignUpProps {

    public static SignUp signUp(boolean isTeacher, String schoolId, boolean agreed){
        SignUp signup = new SignUp();
        signup.setSecurityQuestion("{\"id\":6,\"answer\":\"1111\"}");
        signup.setFirstName("Gilberto" + new Random().nextInt(1000));
        signup.setLastName("Sanchez" + new Random().nextInt(1000));
        signup.setEmail("gilberto" + new Random().nextInt(1000) + "@thecoursekey.com");
        signup.setPassword("Test619");
        //Optional
        signup.setIsTeacher(isTeacher);
        signup.setIssuedID(String.valueOf(new Random().nextInt(1000)));
        signup.setSchoolID(schoolId);
        signup.setHasAgreedToTerms(agreed);
        return signup;
    }

    public static SignUp signUp400(String schoolId){
        SignUp signup = new SignUp();
        signup.setSecurityQuestion("{\"id\":6,\"answer\":\"1111\"}");
        signup.setFirstName("Gilberto");
        signup.setLastName("Sanchez");
        signup.setEmail("gilberto@thecoursekey.com");
        signup.setPassword("Test619");
        //Optional
        signup.setIsTeacher(false);
        signup.setIssuedID(String.valueOf(new Random().nextInt(1000)));
        signup.setSchoolID(schoolId);
        signup.setHasAgreedToTerms(false);
        return signup;
    }
    public static String signUp500(){
        String requestInternalServerError =
                "{" +
                        "\"securityQuestion\":\"{\"id\":6,\"answer\":\"1111\"}\"," +
                        "\"firstName\":\"Mike2\"," +
                        "\"lastName\":\"Woo2\"," +
                        "\"password\":\"zzzzzzz\"," +
                        "\"email\":\"michaelpwoo@gmail.com\"," +
                        "\"isTeacher\":hhhh," +
                        "\"issuedID\":\"1234\"," +
                        "\"schoolID\":\"1151\"," +
                        "\"hasAgreedToTerms\": false" +
                "}";
        return requestInternalServerError;
    }
}
