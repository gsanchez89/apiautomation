package com.coursekey.properties.auth;

import com.thecoursekey.models.auth.ResetPassword;

public class ResetPasswordData {
    public static ResetPassword resetPassword(String oldPass,String newPass){
        ResetPassword resetpassword = new ResetPassword();
        resetpassword.setOldPass(oldPass);
        resetpassword.setNewPass(newPass);
        return resetpassword;
    }
    public static ResetPassword resetPassword400(){
        ResetPassword resetpassword = new ResetPassword();
        resetpassword.setOldPass("Cool");
        resetpassword.setNewPass("Test619");
        return resetpassword;
    }
}
