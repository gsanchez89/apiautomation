package com.thecoursekey.models.auth;

public class ValidationEmail {

    private int userID;
    private String verificationCode;

    public int getUserID() { return userID; }

    public void setUserID(int userID) { this.userID = userID; }

    public String getVerificationCode() { return verificationCode; }

    public void setVerificationCode(String verificationCode) { this.verificationCode = verificationCode; }

}
