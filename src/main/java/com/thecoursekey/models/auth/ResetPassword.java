package com.thecoursekey.models.auth;

public class ResetPassword {

    String oldPass;
    String newPass;

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

    public String getOldPass() {
        return oldPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public String getNewPass() {
        return newPass;
    }

}
