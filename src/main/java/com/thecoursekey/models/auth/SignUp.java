package com.thecoursekey.models.auth;

public class SignUp {
    private String securityQuestion;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private boolean isTeacher;
    private String issuedID;
    private String schoolID;
    private boolean hasAgreedToTerms;

    public String getSecurityQuestion() {
        return securityQuestion;
    }

    public void setSecurityQuestion(String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getIsTeacher() {
        return isTeacher;
    }

    public void setIsTeacher(boolean teacher) {
        isTeacher = teacher;
    }

    public String getIssuedID() {
        return issuedID;
    }

    public void setIssuedID(String issuedID) {
        this.issuedID = issuedID;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) { this.schoolID = schoolID; }

    public boolean isHasAgreedToTerms() {
        return hasAgreedToTerms;
    }

    public void setHasAgreedToTerms(boolean hasAgreedToTerms) {
        this.hasAgreedToTerms = hasAgreedToTerms;
    }
}
