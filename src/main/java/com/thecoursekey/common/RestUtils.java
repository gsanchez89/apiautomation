package com.thecoursekey.common;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class RestUtils {
    public static String ENDPOINT;
    public static RequestSpecBuilder REQUEST_BUILDER;
    public static RequestSpecification REQUEST_SPEC;
    public static ResponseSpecBuilder RESPONSE_BUILDER;
    public static ResponseSpecification RESPONSE_SPEC;

    public static void setEndPoint(String epoint) {
        ENDPOINT = epoint;
    }

    public static RequestSpecification getRequestSpecification() {
        REQUEST_BUILDER = new RequestSpecBuilder();
        REQUEST_BUILDER.setBaseUri(PropertyManager.getInstance().getProperty("baseurl"));
        REQUEST_BUILDER.setPort(Integer.parseInt(PropertyManager.getInstance().getProperty("baseport")));
        REQUEST_SPEC = REQUEST_BUILDER.build();
        return REQUEST_SPEC;
    }

    public static ResponseSpecification getResponseSpecification() {
        RESPONSE_BUILDER = new ResponseSpecBuilder();
        RESPONSE_BUILDER.expectStatusCode(200);
        RESPONSE_SPEC = RESPONSE_BUILDER.build();
        return RESPONSE_SPEC;
    }

    public static RequestSpecification createQueryParam(RequestSpecification rspec, String param, String value) {
        return rspec.queryParam(param, value);
    }

    public static RequestSpecification createQueryParams(RequestSpecification rspec, Map<String, String> queryMap) {
        return rspec.queryParams(queryMap);
    }

    public static RequestSpecification createPathParam(RequestSpecification rspec, String param, String value) {
        return rspec.pathParam(param, value);
    }

    public static RequestSpecification createPathParams(RequestSpecification rspec, Map<String, String> pathMap) {
        return rspec.pathParams(pathMap);
    }

    public static RequestSpecification createFormParam(RequestSpecification rspec, String param, String value) {
        return rspec.formParam(param, value);
    }

    public static RequestSpecification createFormParams(RequestSpecification rspec, Map<String, Object> formMap) {
        return rspec.formParams(formMap);
    }

    public static Response getResponse() {
        return given().get(ENDPOINT);
    }

    public static Response getResponse(RequestSpecification reqSpec, RequestEnum request) {
        REQUEST_SPEC.spec(reqSpec);
        Response response = null;
        switch (request){
            case GET:
                response = given().spec(REQUEST_SPEC.header("nimbus", "frieza")).when().log().all().get(ENDPOINT);
                break;
            case POST:
                response = given().spec(REQUEST_SPEC.header("nimbus", "frieza")).when().log().all().post(ENDPOINT);
                break;
            case PUT:
                response = given().spec(REQUEST_SPEC.header("nimbus", "frieza")).when().log().all().put(ENDPOINT);
                break;
            case DELETE:
                response = given().spec(REQUEST_SPEC.header("nimbus", "frieza")).when().log().all().delete(ENDPOINT);
                break;
            default:
                System.err.println("Type is not supported");
        }
        return response;
    }

    public static JsonPath getJsonPath(Response res) {
        String path = res.asString();
        return new JsonPath(path);
    }

    public static XmlPath getXmlPath(Response res) {
        String path = res.asString();
        return new XmlPath(path);
    }

    public static void resetBathPath() {
        RestAssured.basePath = null;
    }

    public static RequestSpecification setContentType(ContentType type) {
        return given().contentType(type);
    }
}
