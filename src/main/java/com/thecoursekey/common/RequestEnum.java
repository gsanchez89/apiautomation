package com.thecoursekey.common;

public enum RequestEnum {
    GET,
    PUT,
    POST,
    DELETE
}
