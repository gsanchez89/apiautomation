package com.thecoursekey.common;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Constants {
    public static final String AUTH = "/api/v2/auth/";

    public static final String HTML_REPORT = System.getProperty("user.dir") + "/results/api_report" +
            new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()) + ".html";
    public static final String EXTENT_CONFIG = System.getProperty("user.dir") + "/extent-config.xml";
}
